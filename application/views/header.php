<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <meta name="description" content="">
    <meta name="author" content="viniciusilveira">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/components/angular/angular-csp.css">

    <script src="assets/components/jquery/dist/jquery.min.js"></script>
    <script src="assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/components/angular/angular.min.js"></script>
</head>
<header>

</header>