# Codeigniter + PHPUnit + Angular + Bootstrap + Font-awesome


Skeleton for use codeigniter


## Server Requirements


Composer, npm, bower, php 5.4 or newer and apache 2.4


## Installation


### 1. Install all dependencies

* Apache

```bash
user@user:~ sudo apt-get install apache2
```

* PHP 5.4 or newer

```bash
user@user:~ sudo apt-get install php5 php5-mysql php-pear php5-gd  php5-mcrypt php5-curl
```

* Postgresql

```bash
user@user:~ sudo apt-get install postgresql postgresql-contrib
```

* Composer

```bash
user@user:~ sudo apt-get install curl php5-cli git
user@user:~ sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

* And bower

```bash
user@user:~ sudo apt-get install nodejs
user@user:~ sudo apt-get install npm
user@user:~ sudo npm install -g bower
user@user:~ sudo ln -s /usr/bin/nodejs /usr/bin/node
```

### 2. Clone this repository

```bash
user@user:~ git clone https://gitlab.com/viniciusilveira/codeigniter.git
```

### 3. Move to /var/www/html/

```bash
user@user:~ sudo mv codeigniter /var/www/mysite
```

### 4. And run composer install and bower install

```shell
user@user:~ $ composer install
user@user:~ $ bower install
```

### Enjoy